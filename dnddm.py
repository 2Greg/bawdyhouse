#    Friendly Telegram (telegram userbot)
#    By Magical Unicorn (based on official Anti PM & AFK Friendly Telegram modules)
#    Copyright (C) 2020 Magical Unicorn

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.

#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.

#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

#    Customised DND module for Denissa Mashka

from .. import loader, utils

import logging
import datetime
import time
import asyncio

from telethon import functions, types

logger = logging.getLogger(__name__)


@loader.tds
class DoNotDisturbMod(loader.Module):
    """
    DND (Do Not Disturb) :
    -> Prevents people sending you unsolicited private messages.
    -> Prevents disturbing when you are unavailable.\n
    Commands :
     
    """
    strings = {"name": "DND",
               "afk": "<b>I'm AFK right now (since</b> <i>{}</i> <b>ago).</b>",
               "afk_back": "<b>I'm goin' BACK !</b>",
               "afk_gone": "<b>I'm goin' AFK !</b>",
               "afk_no_group_off": "<b>AFK status message enabled for group chats.</b>",
               "afk_no_group_on": "<b>AFK status message disabled for group chats.</b>",
               "afk_no_pm_off": "<b>AFK status message enabled for PMs.</b>",
               "afk_no_pm_on": "<b>AFK status message disabled for PMs.</b>",
               "afk_notif_off": "<b>Notifications are now disabled during AFK time.</b>",
               "afk_notif_on": "<b>Notifications are now enabled during AFK time.</b>",
               "afk_rate_limit_off": "<b>AFK status message rate limit disabled.</b>",
               "afk_rate_limit_on": ("<b>AFK status message rate limit enabled.</b>"
                                     "\n\n<b>One AFK status message max will be sent per chat.</b>"),
               "afk_reason": ("<b>I'm AFK right now (since {} ago).</b>"
                              "\n\n<b>Reason :</b> <i>{}</i>"),
               "arg_on_off": "<b>Argument must be 'off' or 'on' !</b>",
               "option_one": "\n\nAvailable payment options"
                             "\n\n• <b>PayPal</b> - dianaelk@outlook.com"
                             "\n\n• <b>Bank Transfer</b>"
                             "\nAc No: 11213400438260"
                             "\nName: PRIYA"
                             "\nBank: RHB\nReference: ODL"
                             "\n\nDo not put any other reference then the one mentioned. Make sure you make a instant transfer. Send me the receipt once you have done it.",
               "option_two": "<b>Bank Transfer</b>"
                             "\n\nAc No: 11213400438260"
                             "\nName: PRIYA"
                             "\nBank: RHB\nReference: ODL"
                             "\n\nDo not put any other reference then the one mentioned. Make sure you make a instant transfer. Send me the receipt once you have done it.",
               "pm_off": ("<b>Automatic answer for denied PMs disabled."
                          "\n\nUsers are now free to PM !</b>"),
               "pm_on": "<b>An automatic answer is now sent for denied PMs.</b>",
               "pm_allowed": "<b>I have allowed</b> <a href='tg://user?id={}'>you</a> <b>to PM now.</b>",
               "pm_blocked": ("<b>I don't want any PM from</b> <a href='tg://user?id={}'>you</a>, "
                              "<b>so you have been blocked !</b>"),
               "pm_denied": "<b>I have denied</b> <a href='tg://user?id={}'>you</a> <b>to PM now.</b>",
               "pm_go_away": ("Hey there babe !\n"
                              "Interested to video call ?\n"
                              "\nI'm Denissa, Verified And Trusted among group members ❤️\n\n"
                              "<b>Video Call Price:</b>"
                              "\nRM 50 - 10 mins"
                              "\nRM 80 - 20 mins"
                              "\nRM 130 - 30 mins"
                              "\nPrivate Telegram group - RM 150\n\n"
                              "<b>Proof of Customers Group :</b>"
                              "\nhttps://t.me/joinchat/vsuQdQ4CZsw3ZDAx"
                              "\n(If you think I'm a scammer read my previous customers' feedback)\n\n"
                              "<b>Available payment options</b>"
                              "\n• PayPal - dianaelk@outlook.com"
                              "\n• Bank Transfer"
                              "\nAc No: 11213400438260"
                              "\nName: PRIYA"
                              "\nBank: RHB"
                              "\nReference: ODL\n"
                              "\nDo not put any other reference than the one mentioned. Make sure you make an instant transfer. Send me the receipt once you have done it."),
               "pm_reported": "<b>You just got reported to spam !</b>",
               "pm_notif_off": "<b>Notifications from denied PMs are now disabled.</b>",
               "pm_notif_on": "<b>Notifications from denied PMs are now enabled.</b>",
               "pm_options": "What do you want babe?"
                             "\n\n1. Video Call with me"
                             "\n2. Private Telegram Group"
                             "\n\nI am busy right now. This is an automated reply. I will write to you soon, just tell me what you need babe. So I can reply you with details once I am back online",
               "pm_option_arg": "Send either one or two with option command.",
               "pm_unblocked": ("<b>Alright fine! I'll forgive them this time. PM has been unblocked for</b> "
                                "<a href='tg://user?id={}'>this user</a>."),
               "unknow": ("An unknow problem as occured."),
               "who_to_allow": "<b>Who shall I allow to PM ?</b>",
               "who_to_block": "<b>Specify who to block.</b>",
               "who_to_deny": "<b>Who shall I deny to PM ?</b>",
               "who_to_report": "<b>Who shall I report ?</b>",
               "who_to_unblock": "<b>Specify who to unblock.</b>"}

    def __init__(self):
        self._me = None
        self.default_pm_limit = 50

    async def client_ready(self, client, db):
        self._db = db
        self._client = client
        self._me = await client.get_me(True)

    async def unafkcmd(self, message):
        """Remove the AFK status.\n """
        self._db.set(__name__, "afk", False)
        self._db.set(__name__, "afk_gone", None)
        self._db.set(__name__, "afk_rate", [])
        await utils.answer(message, self.strings("afk_back", message))

    async def afkcmd(self, message):
        """
        .afk : Enable AFK status.
        .afk [message] : Enable AFK status and add a reason.
         
        """
        if utils.get_args_raw(message):
            self._db.set(__name__, "afk", utils.get_args_raw(message))
        else:
            self._db.set(__name__, "afk", True)
        self._db.set(__name__, "afk_gone", time.time())
        self._db.set(__name__, "afk_rate", [])
        await utils.answer(message, self.strings("afk_gone", message))

    async def afknogroupcmd(self, message):
        """
        .afknogroup : Disable/Enable AFK status message for group chats.
        .afknogroup off : Enable AFK status message for group chats.
        .afknogroup on : Disable AFK status message for group chats.
         
        """
        if utils.get_args_raw(message):
            afknogroup_arg = utils.get_args_raw(message)
            if afknogroup_arg == "off":
                self._db.set(__name__, "afk_no_group", False)
                await utils.answer(message, self.strings("afk_no_group_off"))
            elif afknogroup_arg == "on":
                self._db.set(__name__, "afk_no_group", True)
                await utils.answer(message, self.strings("afk_no_group_on", message))
            else:
                await utils.answer(message, self.strings("arg_on_off", message))
        else:
            afknogroup_current = self._db.get(__name__, "afk_no_group")
            if afknogroup_current is None or afknogroup_current is False:
                self._db.set(__name__, "afk_no_group", True)
                await utils.answer(message, self.strings("afk_no_group_on", message))
            elif afknogroup_current is True:
                self._db.set(__name__, "afk_no_group", False)
                await utils.answer(message, self.strings("afk_no_group_off", message))
            else:
                await utils.answer(message, self.strings("unknow", message))

    async def afknopmcmd(self, message):
        """
        .afknopm : Disable/Enable AFK status message for PMs.
        .afknopm off : Enable AFK status message for PMs.
        .afknopm on : Disable AFK status message for PMs.
         
        """
        if utils.get_args_raw(message):
            afknopm_arg = utils.get_args_raw(message)
            if afknopm_arg == "off":
                self._db.set(__name__, "afk_no_pm", False)
                await utils.answer(message, self.strings("afk_no_pm_off", message))
            elif afknopm_arg == "on":
                self._db.set(__name__, "afk_no_pm", True)
                await utils.answer(message, self.strings("afk_no_pm_on", message))
            else:
                await utils.answer(message, self.strings("arg_on_off", message))
        else:
            afknopm_current = self._db.get(__name__, "afk_no_pm")
            if afknopm_current is None or afknopm_current is False:
                self._db.set(__name__, "afk_no_pm", True)
                await utils.answer(message, self.strings("afk_no_pm_on", message))
            elif afknopm_current is True:
                self._db.set(__name__, "afk_no_pm", False)
                await utils.answer(message, self.strings("afk_no_pm_off", message))
            else:
                await utils.answer(message, self.strings("unknow", message))

    async def afknotifcmd(self, message):
        """
        .afknotif : Disable/Enable the notifications during AFK time.
        .afknotif off : Disable the notifications during AFK time.
        .afknotif on : Enable the notifications during AFK time.
         
        """
        if utils.get_args_raw(message):
            afknotif_arg = utils.get_args_raw(message)
            if afknotif_arg == "off":
                self._db.set(__name__, "afk_notif", False)
                await utils.answer(message, self.strings("afk_notif_off", message))
            elif afknotif_arg == "on":
                self._db.set(__name__, "afk_notif", True)
                await utils.answer(message, self.strings("afk_notif_on", message))
            else:
                await utils.answer(message, self.strings("arg_on_off", message))
        else:
            afknotif_current = self._db.get(__name__, "afk_notif")
            if afknotif_current is None or afknotif_current is False:
                self._db.set(__name__, "afk_notif", True)
                await utils.answer(message, self.strings("afk_notif_on", message))
            elif afknotif_current is True:
                self._db.set(__name__, "afk_notif", False)
                await utils.answer(message, self.strings("afk_notif_off", message))
            else:
                await utils.answer(message, self.strings("unknow", message))

    async def afkratecmd(self, message):
        """
        .afkrate : Disable/Enable AFK rate limit.
        .afkrate off : Disable AFK rate limit.
        .afkrate on : Enable AFK rate limit. One AFK status message max will be sent per chat.
         
        """
        if utils.get_args_raw(message):
            afkrate_arg = utils.get_args_raw(message)
            if afkrate_arg == "off":
                self._db.set(__name__, "afk_rate_limit", False)
                await utils.answer(message, self.strings("afk_rate_limit_off", message))
            elif afkrate_arg == "on":
                self._db.set(__name__, "afk_rate_limit", True)
                await utils.answer(message, self.strings("afk_rate_limit_on", message))
            else:
                await utils.answer(message, self.strings("arg_on_off", message))
        else:
            afkrate_current = self._db.get(__name__, "afk_rate_limit")
            if afkrate_current is None or afkrate_current is False:
                self._db.set(__name__, "afk_rate_limit", True)
                await utils.answer(message, self.strings("afk_rate_limit_on", message))
            elif afkrate_current is True:
                self._db.set(__name__, "afk_rate_limit", False)
                await utils.answer(message, self.strings("afk_rate_limit_off", message))
            else:
                await utils.answer(message, self.strings("unknow", message))

    async def allowcmd(self, message):
        """Allow this user to PM. Reply .allow @username\n """
        user = await utils.get_target(message)
        if not user:
            await utils.answer(message, self.strings("who_to_allow", message))
            return
        self._db.set(__name__, "allow", list(set(self._db.get(__name__, "allow", [])).union({user})))
        await utils.answer(message, self.strings("pm_allowed", message).format(user))

    async def blockcmd(self, message):
        """Block this user to PM without being warned.\n """
        user = await utils.get_target(message)
        if not user:
            await utils.answer(message, self.strings("who_to_block", message))
            return
        await message.client(functions.contacts.BlockRequest(user))
        await utils.answer(message, self.strings("pm_blocked", message).format(user))

    async def denycmd(self, message):
        """Deny this user to PM without being warned.\n """
        user = await utils.get_target(message)
        if not user:
            await utils.answer(message, self.strings("who_to_deny", message))
            return
        self._db.set(__name__, "allow", list(set(self._db.get(__name__, "allow", [])).difference({user})))
        await utils.answer(message, self.strings("pm_denied", message).format(user))

    async def pmcmd(self, message):
        """
        .pm off : Disable automatic answer for denied PMs.
        .pm on : Enable automatic answer for denied PMs.
         
        """
        if utils.get_args_raw(message):
            pm_arg = utils.get_args_raw(message)
            if pm_arg == "off":
                self._db.set(__name__, "pm", True)
                await utils.answer(message, self.strings("pm_off", message))
            elif pm_arg == "on":
                self._db.set(__name__, "pm", False)
                await utils.answer(message, self.strings("pm_on", message))
            else:
                await utils.answer(message, self.strings("arg_on_off", message))
        else:
            pm_current = self._db.get(__name__, "pm")
            if pm_current is None or pm_current is False:
                self._db.set(__name__, "pm", True)
                await utils.answer(message, self.strings("pm_off", message))
            elif pm_current is True:
                self._db.set(__name__, "pm", False)
                await utils.answer(message, self.strings("pm_on", message))
            else:
                await utils.answer(message, self.strings("unknow", message))

    @loader.unrestricted
    async def optioncmd(self, message):
        """
        Gives option for pm users
        .option one : Gives video call payment info and details.
        .option two: Gives private group payment details.
         
        """
        if utils.get_args_raw(message):
            option_arg = utils.get_args_raw(message)
            if option_arg == "one":
                await utils.answer(message, self.strings("option_one", message))
                return
            elif option_arg == "two":
                await utils.answer(message, self.strings("option_two", message))
                return
            else:
                await utils.answer(message, self.strings("pm_option_arg", message))

    async def pmnotifcmd(self, message):
        """
        .pmnotif : Disable/Enable the notifications from denied PMs.
        .pmnotif off : Disable the notifications from denied PMs.
        .pmnotif on : Enable the notifications from denied PMs.
         
        """
        if utils.get_args_raw(message):
            pmnotif_arg = utils.get_args_raw(message)
            if pmnotif_arg == "off":
                self._db.set(__name__, "pm_notif", False)
                await utils.answer(message, self.strings("pm_notif_off", message))
            elif pmnotif_arg == "on":
                self._db.set(__name__, "pm_notif", True)
                await utils.answer(message, self.strings("pm_notif_on", message))
            else:
                await utils.answer(message, self.strings("arg_on_off", message))
        else:
            pmnotif_current = self._db.get(__name__, "pm_notif")
            if pmnotif_current is None or pmnotif_current is False:
                self._db.set(__name__, "pm_notif", True)
                await utils.answer(message, self.strings("pm_notif_on", message))
            elif pmnotif_current is True:
                self._db.set(__name__, "pm_notif", False)
                await utils.answer(message, self.strings("pm_notif_off", message))
            else:
                await utils.answer(message, self.strings("unknow", message))

    async def reportcmd(self, message):
        """Report the user to spam. Use only in PM.\n """
        user = await utils.get_target(message)
        if not user:
            await utils.answer(message, self.strings("who_to_report", message))
            return
        self._db.set(__name__, "allow", list(set(self._db.get(__name__, "allow", [])).difference({user})))
        if message.is_reply and isinstance(message.to_id, types.PeerChannel):
            await message.client(functions.messages.ReportRequest(peer=message.chat_id,
                                                                  id=[message.reply_to_msg_id],
                                                                  reason=types.InputReportReasonSpam()))
        else:
            await message.client(functions.messages.ReportSpamRequest(peer=message.to_id))
        await utils.answer(message, self.strings("pm_reported", message))

    async def unblockcmd(self, message):
        """Unblock this user to PM."""
        user = await utils.get_target(message)
        if not user:
            await utils.answer(message, self.strings("who_to_unblock"))
            return
        await message.client(functions.contacts.UnblockRequest(user))
        await utils.answer(message, self.strings("pm_unblocked", message).format(user))

    async def watcher(self, message):
        user = await utils.get_user(message)
        pm = self._db.get(__name__, "pm")
        if getattr(message.to_id, "user_id", None) == self._me.user_id and (pm is None or pm is False):
            if not user.is_self and not user.bot and not user.verified and not self.get_allowed(message.sender_id):
                await utils.answer(message, self.strings("pm_go_away",message))
                # user only want message to show but not to block anyone
                self._db.set(__name__, "allow", list(set(self._db.get(__name__, "allow", [])).union({message.chat_id})))
                await asyncio.sleep(2)
                #await utils.answer(message, self.strings("pm_options",message))
                #await message.client.send_file(message.chat_id, "/sdcard/BH/bot-server/3-denissa-bot/voice.mp3", voice_note=True)
                await message.client.send_file(message.chat_id, "/sdcard/BH/bot-server/3-denissa-bot/denissa.jpg")
                pm_notif = self._db.get(__name__, "pm_notif")
                if pm_notif is None or pm_notif is False:
                    await message.client.send_read_acknowledge(message.chat_id)
                return
        if message.mentioned or getattr(message.to_id, "user_id", None) == self._me.user_id:
            afk_status = self._db.get(__name__, "afk")
            if user.is_self or user.bot or user.verified or afk_status is False:
                return
            if message.mentioned and self._db.get(__name__, "afk_no_group") is True:
                return
            afk_no_pm = self._db.get(__name__, "afk_no_pm")
            if getattr(message.to_id, "user_id", None) == self._me.user_id and afk_no_pm is True:
                return
            if self._db.get(__name__, "afk_rate_limit") is True:
                afk_rate = self._db.get(__name__, "afk_rate", [])
                if utils.get_chat_id(message) in afk_rate:
                    return
                else:
                    self._db.setdefault(__name__, {}).setdefault("afk_rate", []).append(utils.get_chat_id(message))
                    self._db.save()
            now = datetime.datetime.now().replace(microsecond=0)
            gone = datetime.datetime.fromtimestamp(self._db.get(__name__, "afk_gone")).replace(microsecond=0)
            diff = now - gone
            if afk_status is True:
                afk_message = self.strings("afk", message).format(diff)
            elif afk_status is not False:
                afk_message = self.strings("afk_reason", message).format(diff, afk_status)
            await utils.answer(message, afk_message)
            afk_notif = self._db.get(__name__, "afk_notif")
            if afk_notif is None or afk_notif is False:
                await message.client.send_read_acknowledge(message.chat_id)

    def get_allowed(self, id):
        return id in self._db.get(__name__, "allow", [])

    def get_current_pm_limit(self):
        pm_limit = self._db.get(__name__, "pm_limit_max")
        if not isinstance(pm_limit, int) or pm_limit < 5 or pm_limit > 1000:
            pm_limit = self.default_pm_limit
            self._db.set(__name__, "pm_limit_max", pm_limit)
        return pm_limit

#updated