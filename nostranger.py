# AutoBlackList module from @Fl1yd
# Translated and renamed to NoStranger by @One_Qone
from .. import loader, utils
from telethon.tl.functions.contacts import BlockRequest
from telethon.tl.functions.messages import ReportSpamRequest


def register(cb):
    cb(NoStrangerMod())

class NoStrangerMod(loader.Module):
    """No stranger can reach to you in private message.All strangers are blocked and reported automatically.If anyone has to text you then you must add that person as contact."""
    strings = {'name': 'NoStranger'}

    async def client_ready(self, client, db):
        self.db = db
    
    async def nostrangercmd(self, message):
        """Turn NoStranger mode on/off
           .nostranger <custom message> set your custom message as reply to strangers or else default message is sent"""
        args = utils.get_args_raw(message)
        nostranger = self.db.get("NoStranger", "status", False)
        if args:
            self.db.set("NoStranger", "status", True)
            self.db.set("NoStranger", "message", str(args))
            return await message.edit("<b>[NoStranger Mode]</b> Аctivated!")
    
        if nostranger == False:
            self.db.set("NoStranger", "status", True)
            self.db.set("NoStranger", "message", "<b>Hi, Sorry to block you.I don't talk to anyone who are not in my contact list.</b>")
            return await message.edit("<b>[NoStranger Mode]</b> Аctivated!")
        self.db.set("NoStranger", "status", False)
        return await message.edit("<b>[NoStranger Mode]</b> Deactivated!")


    async def nostrangerstatcmd(self, message):
        """Get status of NoStranger mode."""
        await message.edit(f"<b>[NoStranger Mode - Status]</b>\n\n"
                           f"<b>Strangers are blocked</b> - {self.db.get('NoStranger', 'status')}\n"
                           f"<b>Chats are deleted</b> - {self.db.get('NoStranger', 'delchat')}")


    async def autodelchatcmd(self, message):
        """Аlso automatically delete stranger's chat."""
        autodel = self.db.get("NoStranger", "delchat", False)
        if autodel == False:
            self.db.set("NoStranger", "delchat", True)
            return await message.edit("<b>[NoStranger Mode - DelChat]</b> Аctivated!")
        self.db.set("NoStranger", "delchat", False)
        return await message.edit("<b>[NoStranger Mode - DelChat]</b> Deactivated!")


    async def watcher(self, message):
        """Watching all pm"""
        try:
            if message.sender_id == (await message.client.get_me()).id: return
            if self.db.get("NoStranger", "status", True):
                if message.is_private and message.sender_id != 777000:
                    user = await message.client.get_entity(message.chat_id)
                    if user.contact == False and user.bot == False:
                        await bot.send_message(message.chat_id, (self.db.get("NoStranger", "message")))
                        await message.client(BlockRequest(message.chat_id))
                        await message.client(ReportSpamRequest(message.chat_id))
                        if self.db.get("NoStranger", "delchat") == True:
                            await message.client.delete_dialog(message.chat_id)
        except (AttributeError, TypeError): pass