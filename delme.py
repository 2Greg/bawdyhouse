from .. import loader, utils

class DelmeMod(loader.Module):
	"""Deletes all messages of user in chat"""
	strings = {'name': 'DelMe'}
	def __init__(self):
		self.name = self.strings['name']
	async def client_ready(self, client, db):
		self.client = client
	
	@loader.sudo
	async def delmecmd(self, message):
		"""Removes all messages from you"""
		chat = message.chat
		if chat:
			args = utils.get_args_raw(message)
			if args != str(message.chat.id+message.sender_id):
				await message.edit(f"<b>If you really want to do this, then write:</b>\n<code>.delme {message.chat.id+message.sender_id}</code>")
				return
			all = (await self.client.get_messages(chat, from_user="me")).total
			messages = [msg async for msg in self.client.iter_messages(chat, from_user="me")]
			await message.edit(f"<b>{all} messages will be deleted!</b>")
			_ = ""
			async for msg in self.client.iter_messages(chat, from_user="me"):
				if _:
					await msg.delete()
				else:
					_ = "_"
			await message.delete()
		else:
			await message.edit("<b>It doesn't work here!</b>")
			
	